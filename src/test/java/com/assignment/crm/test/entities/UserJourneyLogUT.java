package com.assignment.crm.test.entities;

import com.assignment.crm.entities.UserJourneyLog;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserJourneyLogUT {

    @Test
    public void testUserJourneyLogConstructorWithParameters(){

        UserJourneyLog userJourneyLog = new UserJourneyLog(1, LocalDate.now(),"test");
        assertEquals(1, userJourneyLog.getId());
        assertEquals(LocalDate.now(), userJourneyLog.getDate_of_activity());
        assertEquals("test", userJourneyLog.getCommentary());
        }
}
