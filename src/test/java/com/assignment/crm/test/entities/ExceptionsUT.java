package com.assignment.crm.test.entities;

import com.assignment.crm.exceptions.CustomExceptions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExceptionsUT {

        @Test
        public void testCustomException(){

            CustomExceptions customExceptions = new CustomExceptions("Error test");
            assertEquals("Error test", customExceptions.getMessage());


        }
}
