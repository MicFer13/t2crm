package com.assignment.crm.test.entities;

import com.assignment.crm.entities.Logs;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogsUT {

    @Test
    public void logparser(){
        Logs logs = new Logs("Test");
        assertEquals("Test", logs.getActions());
    }
}
