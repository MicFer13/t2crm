package com.assignment.crm.test.entities;

import com.assignment.crm.entities.UserFromUI;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserFromUIUT {

    @Test()
    public void testUserFromUiConstructorWithParameters(){
        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
                );

        assertEquals("Bob", user.getFirstname());
        assertEquals("Job",  user.getSurname());
        assertEquals("M",  user.getGender());
        assertEquals(LocalDate.now(), user.getDate_of_birth());
        assertEquals("test@test.com", user.getEmail_address());
        assertEquals("1 Disc Drive",  user.getAddress());
        assertEquals("NJ1234",  user.getZipcode());
        assertEquals("07851651651651",  user.getPhone_number());
        assertEquals("Fare", user.getMethod_of_contact());
    }
}
