package com.assignment.crm.test.entities;

import com.assignment.crm.entities.User;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserUT{

    @Test()
    public void testUserConstructorWithParameters(){
        User user = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

    assertEquals("Bob", user.getFirstname());
    assertEquals("Job",  user.getSurname());
    assertEquals("M",  user.getGender());
    assertEquals(LocalDate.now(), user.getInitial_date());
    assertEquals("test@test.com", user.getEmail_address());
    assertEquals("1 Disc Drive",  user.getAddress());
    assertEquals("NJ1234",  user.getZipcode());
    assertEquals("07851651651651",  user.getPhone_number());
    assertEquals("Fare", user.getMethod_of_contact());
    assertEquals(LocalDate.now(),  user.getDate_of_birth());
    assertEquals(false, user.isEmail_sent());
    assertEquals(false, user.isEmail_returned());
    assertEquals(false,  user.isPhone_call_made());
    assertEquals(false, user.isPhone_call_returned());
    assertEquals("1", user.getPicture_location());
    assertEquals(false, user.isMeeting_completed());
    assertEquals("", user.getProduct_selected());
    assertEquals("lead", user.getStatus());
    assertEquals(true, user.isActive());
    }
}
