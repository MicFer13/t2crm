package com.assignment.crm.services;

import com.assignment.crm.dao.IUserDAO;
import com.assignment.crm.dao.IUserJourneyLogDAO;
import com.assignment.crm.entities.UserFromUI;
import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.services.logs.IUserJourneyLogService;
import com.assignment.crm.services.user.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
public class UserJounreyLogServiceUT {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IUserJourneyLogService iUserJourneyLogService;

    @Autowired
    IUserService iUserService;

    @BeforeEach
    public void setup(){
        iUserService.deleteAll();
    }

    @Test
    public void saveUserLogToDB(){
        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        List<UserJourneyLog> actual = iUserJourneyLogService.retrieveAllLogsById(1);
        assertEquals("Added to system on "+ LocalDate.now()+",",actual.get(0).getCommentary());
    }
}
