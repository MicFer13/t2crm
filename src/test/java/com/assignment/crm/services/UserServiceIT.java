package com.assignment.crm.services;

import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;

import com.assignment.crm.services.user.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserServiceIT {

    @Autowired
    IUserService iUserService;
    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setup(){
        iUserService.deleteAll();
    }

    @Test
    public void saveUserToDB(){

        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        User expected = iUserService.findById(1);
        assertNotNull(expected);
    }

    @Test
    public void saveTwoUsersToDB(){

        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        UserFromUI usertwo = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);
        iUserService.save(usertwo);

        User expected = iUserService.findById(1);
        assertNotNull(expected);
        User expectedtwo = iUserService.findById(2);
        assertNotNull(expectedtwo);
    }

    @Test
    public void findByIDNewUser(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserService.save(expected);
        User actual =  iUserService.findById(1);
        assertEquals(updated.toString(), actual.toString());
    }

    @Test
    public void findByFirstNameNewUser(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserService.save(expected);
        List<User> actual =  iUserService.findByFirstName("Bob");
        assertEquals(updated.toString(), actual.get(0).toString());
    }

    @Test
    public void findByStatusNewUser(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserService.save(expected);
        List<User> actual =  iUserService.findByStatus("lead");
        assertEquals(updated.toString(), actual.get(0).toString());
    }

    @Test
    public void findAllUsers(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(expected);
        List<User> actual =  iUserService.findAllUsers();
        assertEquals(1, actual.size());
    }

    @Test
    public void updateUser(){
        UserFromUI initial = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(initial);

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "testasdad@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);
        iUserService.updateUser(updated);

        List<User> actual =  iUserService.findAllUsers();
        assertEquals(updated.toString(), actual.get(0).toString());
    }

    @Test
    public void deleteUserById(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserService.save(expected);
        List<User> added =  iUserService.findByStatus("lead");
        assertEquals(updated.toString(), added.get(0).toString());
        iUserService.deleteUserById(1);
        List<User> actual =  iUserService.findByStatus("lead");
        assertEquals(0, actual.size());
    }

    @Test
    public void deleteAllUSer(){
        UserFromUI expected = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserService.save(expected);
        List<User> added =  iUserService.findByStatus("lead");
        assertEquals(updated.toString(), added.get(0).toString());
        iUserService.deleteUserById(1);
        iUserService.deleteAll();
        List<User> actual = iUserService.findAllUsers();
        assertEquals(0, actual.size());
    }

}
