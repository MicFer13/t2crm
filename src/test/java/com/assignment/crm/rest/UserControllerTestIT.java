package com.assignment.crm.rest;


import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;
import com.assignment.crm.services.user.IUserService;
import com.assignment.crm.utility.CRMProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SuppressWarnings("unchecked")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTestIT {

    @LocalServerPort
    int port;

    @Autowired
    CRMProperties crmProperties;

    @Autowired
    IUserService iUserService;

    @Autowired
    MongoTemplate mongoTemplate;

    private String url;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    HttpEntity entity = new HttpEntity(headers);

    @BeforeEach
    public void setup() {
        url = crmProperties.getUrl() + port +"/api";
        iUserService.deleteAll();
    }

    @Test
    public void getUserByFirstName(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);


        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        ResponseEntity<User[]> response = restTemplate.getForEntity(
                url +"/firstname?firstname=Bob",
                User[].class
        );

        assertEquals(expected.toString(), response.getBody()[0].toString());
    }

    @Test
    public void getUserById(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);


        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        ResponseEntity<User> response = restTemplate.getForEntity(
                url +"/id?id=1",
                User.class
        );

      assertEquals(expected.toString(), response.getBody().toString());
    }

    @Test
    public void getStatusLead(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);


        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        ResponseEntity<User[]> response = restTemplate.getForEntity(
                url +"/status?status=lead",
                User[].class
        );

        assertEquals(expected.toString(), expected.toString());
    }

    @Test
    public void getAllusers(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);


        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        ResponseEntity<User[]> response = restTemplate.getForEntity(
                url +"/all",
                User[].class
        );

        assertEquals(expected.toString(), expected.toString());
    }

    @Test
    public void deleteall(){


        UserFromUI user = new UserFromUI(
                "Bob","Job","M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare"
        );

        iUserService.save(user);

        ResponseEntity<User> response = restTemplate.exchange(
                url +"/deleteall",
                HttpMethod.POST,
                entity,
                User.class
        );

        assertEquals( response.getBody(), response.getBody());
    }

}
