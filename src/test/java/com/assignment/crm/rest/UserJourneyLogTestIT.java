package com.assignment.crm.rest;

import com.assignment.crm.entities.Logs;
import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;
import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.services.logs.IUserJourneyLogService;
import com.assignment.crm.services.user.IUserService;
import com.assignment.crm.utility.CRMProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SuppressWarnings("unchecked")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserJourneyLogTestIT {
    @LocalServerPort
    int port;

    @Autowired
    CRMProperties crmProperties;

    @Autowired
    IUserJourneyLogService iUserJourneyLogService;

    @Autowired
    IUserService iUserService;

    @Autowired
    MongoTemplate mongoTemplate;

    private String url;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    HttpEntity entity = new HttpEntity(headers);

    @BeforeEach
    public void setup() {
        url = crmProperties.getUrl() + port +"/api";
        iUserService.deleteAll();
    }

    @Test
    public void getUserByFirstName() {

        UserFromUI user = new UserFromUI(
                "Bob", "Job", "M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651", "Fare"
        );

        iUserService.save(user);

        ResponseEntity<UserJourneyLog[]> response = restTemplate.getForEntity(
                url + "/allLogs/?id=1",
                UserJourneyLog[].class
        );
        assertEquals("Added to system on " + LocalDate.now() + ",", response.getBody()[0].getCommentary());
    }

    @Test
    public void getLogsForUser() {

        UserFromUI user = new UserFromUI(
                "Bob", "Job", "M", LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651", "Fare"
        );

        iUserService.save(user);

        ResponseEntity<Logs[]> response = restTemplate.getForEntity(
                url + "/logsparse/?id=1",
                Logs[].class
        );
        assertEquals("Added to system on","Added to system on");
    }
}
