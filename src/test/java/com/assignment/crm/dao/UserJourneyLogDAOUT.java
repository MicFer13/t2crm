package com.assignment.crm.dao;


import com.assignment.crm.entities.UserJourneyLog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserJourneyLogDAOUT {
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IUserJourneyLogDAO iUserJourneyLogDAO;

    @Autowired
    IUserDAO iUserDAO;

    @BeforeEach
    public void setup(){
        iUserDAO.deleteAll();
    }

    @Test
    public void saveUserLogToDB(){
        iUserJourneyLogDAO.addUserJourneyLogInitial(1);
        List<UserJourneyLog> actual = iUserJourneyLogDAO.retrieveAllLogsById(1);
        assertEquals("Added to system on "+ LocalDate.now()+",",actual.get(0).getCommentary());
    }

    @Test
    public void updateUserLogToDB(){
        iUserJourneyLogDAO.addUserJourneyLogInitial(1);
        iUserJourneyLogDAO.updateUserJourneyLog(1,"test");
        List<UserJourneyLog> actual = iUserJourneyLogDAO.retrieveAllLogsById(1);
        assertEquals("Added to system on "+ LocalDate.now()+",Date of Action(s)"+ LocalDate.now()+",test", actual.get(0).getCommentary());
    }

    @Test
    public void retrieveAllLogByIdFromDB(){
        iUserJourneyLogDAO.addUserJourneyLogInitial(1);
        List<UserJourneyLog> actual = iUserJourneyLogDAO.retrieveAllLogsById(1);
        assertEquals(1,actual.size());
    }


    @Test
    public void retrieveLog(){

        String log = "Added to system on 2020-10-08,Date of Action(s)2020-10-08," +
                "[ValueChange{ 'email_address' value changed from 'test@test.com' to 'testemail@test.com' }," +
                " ValueChange{ 'address' value changed from '1 Disc Drive' to '2 Disc Drive' }," +
                " ValueChange{ 'email_sent' value changed from 'false' to 'true' }]Date of Action(s)2020-10-08," +
                "[]Date of Action(s)2020-10-08,[ValueChange{ 'email_returned' value changed from 'false' to 'true' }]";

       log = log.replace("[","");
        log = log.replace("]","");
        log = log.replace("{","");
        log = log.replace("}","");

        String part[] = log.split(",");
        String temp ="";




    }


}
