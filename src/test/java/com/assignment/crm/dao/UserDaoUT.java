package com.assignment.crm.dao;


import com.assignment.crm.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserDaoUT {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IUserDAO iUserDAO;

    @BeforeEach
    public void setup(){
        iUserDAO.deleteAll();
    }

    @Test
    public void saveUserToDB(){

        User user = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(user);

        User expected = iUserDAO.findById(1);
        assertNotNull(expected);
    }

    @Test
    public void saveTwoUsersToDB(){

        User user = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        User usertwo = new User(  2,
                "Bob","Job","M",false,"",LocalDate.now(),
                "testagain@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(user);
        iUserDAO.save(usertwo);

        User expected = iUserDAO.findById(1);
        assertNotNull(expected);
        User expectedtwo = iUserDAO.findById(2);
        assertNotNull(expectedtwo);
    }

    @Test
    public void findByIDNewUser(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(expected);
        User actual =  iUserDAO.findById(1);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void findByFirstNameNewUser(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(expected);
        List<User> actual =  iUserDAO.findByFirstName("Bob");
        assertEquals(expected.toString(), actual.get(0).toString());
    }

    @Test
    public void findByStatusNewUser(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(expected);
        List<User> actual =  iUserDAO.findByStatus("lead");
        assertEquals(expected.toString(), actual.get(0).toString());
    }

    @Test
    public void findAllUsers(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(expected);
        List<User> actual =  iUserDAO.findAllUsers();
        assertEquals(1, actual.size());
    }

    @Test
    public void updateUser(){
        User initial = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(initial);

        User updated = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);
        iUserDAO.updateUser(updated);

        List<User> actual =  iUserDAO.findAllUsers();
        assertEquals(updated.toString(), actual.get(0).toString());
    }

    @Test
    public void deleteUserById(){
        User expected = new User(  1,
                "Bob","Job","M",false,"",LocalDate.now(),
                "test@test.com", "1 Disc Drive", "NJ1234",
                "07851651651651","Fare",
                LocalDate.now(),
                false,
                false,
                false,
                false,
                "1",
                "lead",
                true);

        iUserDAO.save(expected);
        List<User> added =  iUserDAO.findByStatus("lead");
        assertEquals(expected.toString(), added.get(0).toString());
        iUserDAO.deleteUserById(1);
        List<User> actual =  iUserDAO.findByStatus("lead");
        assertEquals(0, actual.size());
    }

    @Test
    public void test(){
       String log  = "Added to system on 2020-10-08," +
                "Date of Action(s)2020-10-08," +
                "[ValueChange{ 'email_address' value changed from 'test@test.com' to 'testemail@test.com' }, " +
                "ValueChange{ 'address' value changed from '1 Disc Drive' to '2 Disc Drive' }," +
                " ValueChange{ 'email_sent' value changed from 'false' to 'true' }]Date of Action(s)2020-10-08," +
                "[]Date of Action(s)2020-10-08,[ValueChange{ 'email_returned' value changed from 'false' to 'true' }]";
   log = log.replace("[","");
        log = log.replace("]","");
        log = log.replace("{","");
        log = log.replace("}","");
        log = log.replace("ValueChange","");



        String [] items = log.split(",");

        List<String> myStringList = new ArrayList<String>(items.length);
        for (String s:items) {

            myStringList.add( s );
        }
        String test = "";
    }


}
