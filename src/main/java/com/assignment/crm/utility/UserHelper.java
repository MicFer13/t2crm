package com.assignment.crm.utility;

import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
@Component
public class UserHelper {

    private CRMProperties cRMProperties;

    public UserHelper(CRMProperties cRMProperties){
       this.cRMProperties = cRMProperties;
    }

    public User combineUIandFullUser(UserFromUI userFromUI){
        return
                new User(
                        cRMProperties.getZero(),
                        userFromUI.getFirstname(),
                        userFromUI.getSurname(),
                        userFromUI.getGender(),
                        cRMProperties.isActionsdefault(),
                        "",
                        userFromUI.getDate_of_birth(),
                        userFromUI.getEmail_address(),
                        userFromUI.getAddress(),
                        userFromUI.getZipcode(),
                        userFromUI.getPhone_number(),
                        userFromUI.getMethod_of_contact(),
                        LocalDate.now(),
                        cRMProperties.isActionsdefault(),
                        cRMProperties.isActionsdefault(),
                        cRMProperties.isActionsdefault(),
                        cRMProperties.isActionsdefault(),
                        userFromUI.getEmail_address(),
                        cRMProperties.getStatusdefault(),
                        cRMProperties.isActivedefault());
    }
}
