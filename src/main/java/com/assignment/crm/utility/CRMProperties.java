package com.assignment.crm.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CRMProperties {

    @Value("${crm.meeting_completed}")
    private String meeting_completed;

    @Value("${crm.product_selected}")
    private String product_selected;

    @Value("${crm.actionsdefault}")
    private boolean actionsdefault;

    @Value("${crm.activedefault}")
    private boolean activedefault;

    @Value("${crm.statusdefault}")
    private String statusdefault;

    @Value("${crm.id}")
    private String id;

    @Value("${crm.email_address}")
    private String email_address;

    @Value("${crm.address}")
    private String address;

    @Value("${crm.zipcode}")
    private String zipcode;

    @Value("${crm.phone_number}")
    private String phone_number;

    @Value("${crm.email_sent}")
    private String email_sent;

    @Value("${crm.email_returned}")
    private String email_returned;

    @Value("${crm.phone_call_made}")
    private String phone_call_made;

    @Value("${crm.phone_call_returned}")
    private String phone_call_returned;

    @Value("${crm.status}")
    private String status;

    @Value("${crm.active}")
    private String active;

    @Value("${crm.firstname}")
    private String firstname;

    @Value("${crm.one}")
    private int one;

    @Value("${crm.zero}")
    private int zero;

    @Value("${crm.dateofaction}")
    private String dateofaction;

    @Value("${crm.commentary}")
    private String commentary;

    @Value("${crm.date_of_activity}")
    private String date_of_activity;

    @Value("${crm.addedsys}")
    private String addedsys;

    @Value("${crm.url}")
    private String url;

    public String getUrl() {
        return url;
    }

    public boolean isActionsdefault() {
        return actionsdefault;
    }

    public boolean isActivedefault() {
        return activedefault;
    }

    public String getStatusdefault() {
        return statusdefault;
    }

    public String getId() {
        return id;
    }

    public String getEmail_address() {
        return email_address;
    }

    public String getAddress() {
        return address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getEmail_sent() {
        return email_sent;
    }

    public String getEmail_returned() {
        return email_returned;
    }

    public String getPhone_call_made() {
        return phone_call_made;
    }

    public String getPhone_call_returned() {
        return phone_call_returned;
    }

    public String getStatus() {
        return status;
    }

    public String getActive() {
        return active;
    }

    public int getOne() {
        return one;
    }

    public int getZero() {
        return zero;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getDateofaction() {
        return dateofaction;
    }

    public String getCommentary() {
        return commentary;
    }

    public String getDate_of_activity() {
        return date_of_activity;
    }

    public String getAddedsys() {
        return addedsys;
    }


    public String getProduct_selected() {
        return product_selected;
    }

    public String getMeeting_completed() {
        return meeting_completed;
    }
}
