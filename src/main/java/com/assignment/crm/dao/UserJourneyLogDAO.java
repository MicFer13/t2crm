package com.assignment.crm.dao;

import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.utility.CRMProperties;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class UserJourneyLogDAO implements IUserJourneyLogDAO {
    private MongoTemplate mongoTemplate;
    private CRMProperties crmProperties;

    public UserJourneyLogDAO(MongoTemplate mongoTemplate,CRMProperties crmProperties) {
        this.crmProperties = crmProperties;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void updateUserJourneyLog(int id, String commentary) {
        String amendedCommentary = retrieveAllLogsById(id).get(crmProperties.getZero()).getCommentary();
        amendedCommentary = amendedCommentary +  crmProperties.getDateofaction() +LocalDate.now() +","+ commentary  ;
        Query query = new Query();
        query.addCriteria(Criteria.where(crmProperties.getId()).is(id));
        Update update = new Update();
        update.set(crmProperties.getDate_of_activity(), LocalDate.now());
        update.set(crmProperties.getCommentary(),amendedCommentary);

        mongoTemplate.updateFirst(query, update, UserJourneyLog.class);

    }

    @Override
    public void addUserJourneyLogInitial(int id) {
        UserJourneyLog userJourneyLog = new UserJourneyLog(id, LocalDate.now(),crmProperties.getAddedsys() + LocalDate.now() +",");
        mongoTemplate.insert(userJourneyLog);
    }

    @Override
    public List<UserJourneyLog> retrieveAllLogsById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        List<UserJourneyLog> userJourneyLog = mongoTemplate.find(query, UserJourneyLog.class);
        return userJourneyLog;
    }
}