package com.assignment.crm.dao;

import com.assignment.crm.entities.User;

import java.util.List;

public interface IUserDAO {

    User findById(int id);
    List<User> findAllUsers();
    List<User> findByFirstName(String firstname);
    List<User> findByStatus(String status);
    void save(User user);
    void updateUser(User user);
    void deleteUserById(int id);
    void deleteAll();
}
