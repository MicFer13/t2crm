package com.assignment.crm.dao;

import com.assignment.crm.entities.User;

import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.utility.CRMProperties;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAO implements IUserDAO{

    private MongoTemplate mongoTemplate;
    private UserJourneyLogDAO userJourneyLogDAO;
    Javers javers = JaversBuilder.javers().build();
    private CRMProperties cRMProperties;

    public UserDAO(MongoTemplate mongoTemplate, UserJourneyLogDAO userJourneyLogDAO,
                   CRMProperties cRMProperties){
        this.mongoTemplate = mongoTemplate;
        this.userJourneyLogDAO = userJourneyLogDAO;
        this.cRMProperties = cRMProperties;
    }

    @Override
    public User findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where(cRMProperties.getId()).is(id));
        User user = mongoTemplate.findOne(query, User.class);
        return user;
    }

    @Override
    public List<User> findAllUsers() {
        Query query = new Query();
        query.addCriteria(Criteria.where(cRMProperties.getId()).exists(true));
        List<User> userList = mongoTemplate.find(query, User.class);
        return userList;
    }

    @Override
    public List<User> findByFirstName(String firstname) {
        Query query = new Query();
        query.addCriteria(Criteria.where(cRMProperties.getFirstname()).is(firstname));
        List<User> userList = mongoTemplate.find(query, User.class);
        return userList;
    }

    @Override
    public List<User> findByStatus(String status) {
        Query query = new Query();
        query.addCriteria(Criteria.where(cRMProperties.getStatus()).is(status));
	query.addCriteria(Criteria.where("active").is(true));
        List<User> userList = mongoTemplate.find(query, User.class);
        return userList;
    }

    @Override
    public void save(User user) {
        Query query = new Query();
        Query condition = query.with(Sort.by(Sort.Direction.DESC, "_"+cRMProperties.getId())).limit(cRMProperties.getOne());

        List<User> myClassList =  mongoTemplate.find(condition, User.class);
        if(myClassList.size() > cRMProperties.getZero() ) {
            user.setId(myClassList.get(cRMProperties.getZero()).getId()+cRMProperties.getOne());
            user.setPicture_location(String.valueOf(user.getId()));
        }
        else{
            user.setId(cRMProperties.getOne());
            user.setPicture_location(String.valueOf(user.getId()));
        }
        mongoTemplate.insert(user);
        userJourneyLogDAO.addUserJourneyLogInitial(user.getId());
    }

    @Override
    public void updateUser(User user) {

        User oldUser = findById(user.getId());
        oldUser.setDate_of_birth(user.getDate_of_birth());
        oldUser.setFirstname(user.getFirstname());
        oldUser.setSurname(user.getSurname());
        oldUser.setGender(user.getGender());
        oldUser.setInitial_date(user.getInitial_date());
        oldUser.setMethod_of_contact(user.getMethod_of_contact());
        oldUser.setPicture_location(user.getPicture_location());

        String commentary = String.valueOf(updatedFieldForLog(oldUser, user));
        userJourneyLogDAO.updateUserJourneyLog(user.getId(), commentary);

        Query query = new Query();
        query.addCriteria(Criteria.where(cRMProperties.getId()).is(user.getId()));
        Update update = new Update();
        update.set(cRMProperties.getEmail_address(), user.getEmail_address());
        update.set(cRMProperties.getEmail_address(), user.getEmail_address());
        update.set(cRMProperties.getEmail_address(), user.getEmail_address());
        update.set(cRMProperties.getAddress(), user.getAddress());
        update.set(cRMProperties.getZipcode(), user.getZipcode());
        update.set(cRMProperties.getPhone_number(), user.getPhone_number());
        update.set(cRMProperties.getEmail_sent(), user.isEmail_sent());
        update.set(cRMProperties.getEmail_returned(), user.isEmail_returned());
        update.set(cRMProperties.getPhone_call_made(), user.isPhone_call_made());
        update.set(cRMProperties.getPhone_call_returned(), user.isPhone_call_returned());
        update.set(cRMProperties.getStatus(), user.getStatus());
        update.set(cRMProperties.getActive(), user.isActive());
        update.set(cRMProperties.getMeeting_completed(), user.isMeeting_completed());
        update.set(cRMProperties.getProduct_selected(), user.getProduct_selected());
        update.set(cRMProperties.getActive(), user.isActive());
        mongoTemplate.updateFirst(query, update, User.class);
    }

    @Override
    public void deleteUserById(int id) {
        mongoTemplate.remove(Query.query(Criteria.where("_"+cRMProperties.getId()).is(id)), User.class);
        mongoTemplate.remove(Query.query(Criteria.where("_"+cRMProperties.getId()).is(id)), UserJourneyLog.class);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getDb().drop();
    }

    private List<String> updatedFieldForLog(User oldMember, User newMember ){
       List<String>mylist = new ArrayList<>();

        Diff diff = javers.compare(oldMember, newMember);
        for(Change change:  diff.getChanges()) {
            System.out.println(change);
            mylist.add(change.toString());
        }
        return mylist;
    }
}
