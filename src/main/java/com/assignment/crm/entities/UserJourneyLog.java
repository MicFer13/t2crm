package com.assignment.crm.entities;

import java.time.LocalDate;

public class UserJourneyLog {
    private int id;
    private LocalDate date_of_activity;
    private String commentary;

    public UserJourneyLog(int id, LocalDate date_of_activity, String commentary) {
        this.id = id;
        this.date_of_activity = date_of_activity;
        this.commentary = commentary;
    }

    public UserJourneyLog() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate_of_activity() {
        return date_of_activity;
    }

    public void setDate_of_activity(LocalDate date_of_activity) {
        this.date_of_activity = date_of_activity;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }
}
