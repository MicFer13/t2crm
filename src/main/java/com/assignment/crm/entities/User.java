package com.assignment.crm.entities;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class User {

    private int id;
    private String firstname;
    private String surname;
    private String gender;
    private boolean meeting_completed;
    private String product_selected;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate date_of_birth;
    private String email_address;
    private String address;
    private String zipcode;
    private String phone_number;
    private String method_of_contact;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate initial_date;
    private boolean email_sent;
    private boolean email_returned;
    private boolean phone_call_made;
    private boolean phone_call_returned;
    private String picture_location;
    private String status;
    private boolean active;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", gender='" + gender + '\'' +
                ", meeting_completed=" + meeting_completed +
                ", product_selected='" + product_selected + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", email_address='" + email_address + '\'' +
                ", address='" + address + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", method_of_contact='" + method_of_contact + '\'' +
                ", initial_date=" + initial_date +
                ", email_sent=" + email_sent +
                ", email_returned=" + email_returned +
                ", phone_call_made=" + phone_call_made +
                ", phone_call_returned=" + phone_call_returned +
                ", picture_location='" + picture_location + '\'' +
                ", status='" + status + '\'' +
                ", active=" + active +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isMeeting_completed() {
        return meeting_completed;
    }

    public void setMeeting_completed(boolean meeting_completed) {
        this.meeting_completed = meeting_completed;
    }

    public String getProduct_selected() {
        return product_selected;
    }

    public void setProduct_selected(String product_selected) {
        this.product_selected = product_selected;
    }

    public LocalDate getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDate date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMethod_of_contact() {
        return method_of_contact;
    }

    public void setMethod_of_contact(String method_of_contact) {
        this.method_of_contact = method_of_contact;
    }

    public LocalDate getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(LocalDate initial_date) {
        this.initial_date = initial_date;
    }

    public boolean isEmail_sent() {
        return email_sent;
    }

    public void setEmail_sent(boolean email_sent) {
        this.email_sent = email_sent;
    }

    public boolean isEmail_returned() {
        return email_returned;
    }

    public void setEmail_returned(boolean email_returned) {
        this.email_returned = email_returned;
    }

    public boolean isPhone_call_made() {
        return phone_call_made;
    }

    public void setPhone_call_made(boolean phone_call_made) {
        this.phone_call_made = phone_call_made;
    }

    public boolean isPhone_call_returned() {
        return phone_call_returned;
    }

    public void setPhone_call_returned(boolean phone_call_returned) {
        this.phone_call_returned = phone_call_returned;
    }

    public String getPicture_location() {
        return picture_location;
    }

    public void setPicture_location(String picture_location) {
        this.picture_location = picture_location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User() {

    }

    public User(int id, String firstname, String surname, String gender, boolean meeting_completed, String product_selected, LocalDate date_of_birth, String email_address, String address, String zipcode, String phone_number, String method_of_contact, LocalDate initial_date, boolean email_sent, boolean email_returned, boolean phone_call_made, boolean phone_call_returned, String picture_location, String status, boolean active) {
        this.id = id;
        this.firstname = firstname;
        this.surname = surname;
        this.gender = gender;
        this.meeting_completed = meeting_completed;
        this.product_selected = product_selected;
        this.date_of_birth = date_of_birth;
        this.email_address = email_address;
        this.address = address;
        this.zipcode = zipcode;
        this.phone_number = phone_number;
        this.method_of_contact = method_of_contact;
        this.initial_date = initial_date;
        this.email_sent = email_sent;
        this.email_returned = email_returned;
        this.phone_call_made = phone_call_made;
        this.phone_call_returned = phone_call_returned;
        this.picture_location = picture_location;
        this.status = status;
        this.active = active;
    }
}

