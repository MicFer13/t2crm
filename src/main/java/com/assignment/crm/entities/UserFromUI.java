package com.assignment.crm.entities;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class UserFromUI {
    private String firstname;
    private String surname;
    private String gender;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate date_of_birth;

    private String email_address;
    private String address;
    private String zipcode;
    private String phone_number;
    private String method_of_contact;



    public UserFromUI(String firstname, String surname, String gender, LocalDate date_of_birth, String email_address, String address, String zipcode, String phone_number, String method_of_contact) {
        this.firstname = firstname;
        this.surname = surname;
        this.gender = gender;
        this.date_of_birth = date_of_birth;
        this.email_address = email_address;
        this.address = address;
        this.zipcode = zipcode;
        this.phone_number = phone_number;
        this.method_of_contact = method_of_contact;
    }

    public UserFromUI(){}

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDate date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMethod_of_contact() {
        return method_of_contact;
    }

    public void setMethod_of_contact(String method_of_contact) {
        this.method_of_contact = method_of_contact;
    }
}
