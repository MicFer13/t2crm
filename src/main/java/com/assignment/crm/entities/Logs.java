package com.assignment.crm.entities;

public class Logs {

    private String actions;
    public Logs(){}

    public Logs(String actions) {
        this.actions = actions;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }
}
