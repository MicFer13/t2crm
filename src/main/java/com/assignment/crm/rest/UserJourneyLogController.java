package com.assignment.crm.rest;

import com.assignment.crm.entities.Logs;
import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.services.logs.IUserJourneyLogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UserJourneyLogController implements IUserJourneyLogController {
    private IUserJourneyLogService service;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserJourneyLogController(IUserJourneyLogService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/allLogs")
    public ResponseEntity<List<UserJourneyLog>> getUserById(int id) {
        logger.info("Calling method getUserById");
        List<UserJourneyLog> myList = service.retrieveAllLogsById(id);

        if (myList.size() < 1) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }

    @Override
    @GetMapping("/logsparse")
    public ResponseEntity<List<Logs>> getlogparseId(int id) {
        logger.info("Calling method getlogparseId");
        List<Logs> myList = service.retrieveParselog(id);

        if (myList.size() < 1) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }
}