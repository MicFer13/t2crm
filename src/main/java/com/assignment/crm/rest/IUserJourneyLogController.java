package com.assignment.crm.rest;

import com.assignment.crm.entities.Logs;
import com.assignment.crm.entities.UserJourneyLog;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface IUserJourneyLogController {
    @GetMapping("/allLogs")
    ResponseEntity<List<UserJourneyLog>> getUserById(@RequestParam int id);
    ResponseEntity<List<Logs>> getlogparseId(@RequestParam int id);
}
