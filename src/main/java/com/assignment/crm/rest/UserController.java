package com.assignment.crm.rest;

import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;
import com.assignment.crm.services.user.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UserController implements IUserController {
    private IUserService service;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserController(IUserService service){
        this.service = service;
    }

    @Override
    @GetMapping("/all")
    public ResponseEntity<List<User>> getAllUsers() {
        logger.info("Calling method getAllUsers");
        List<User> myList = service.findAllUsers();

         if (myList.size()<1){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }

    @Override
    @GetMapping("/id")
    public ResponseEntity<User> getUserById(int id) {
        logger.info("Calling method getUserById");
        User user = service.findById(id);

          if (user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @Override
    @GetMapping("/firstname")
    public ResponseEntity<List<User>> getUserByName(String firstname) {
        logger.info("Calling method getUserByName");
        List<User> myList = service.findByFirstName(firstname);

        if (myList.size()<1){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }

    @Override
    @GetMapping("/status")
    public ResponseEntity<List<User>> getUserByStatus(String status) {
        logger.info("Calling method getUserByStatus");
        List<User> myList = service.findByStatus(status);

        if (myList.size()<1){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }

    @Override
    @PostMapping("/adduser")
    public ResponseEntity<String> addUser(UserFromUI userFromUI) {
        logger.info("Calling method addUser");
        service.save(userFromUI);
        return new ResponseEntity<>("User Created",HttpStatus.OK);
    }

    @Override
    @PostMapping("/updateuser")
    public ResponseEntity<String> updateUser(User user) {
        logger.info("Calling method updateUser");
        service.updateUser(user);
        return new ResponseEntity<>("User Updated",HttpStatus.OK);
    }

    @Override
    @PostMapping("/deleteuser")
    public ResponseEntity<User> deleteUserById(int id) {
        logger.info("Calling method deleteUserById");

        service.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @PostMapping("/deleteall")
    public ResponseEntity<User> deleteAll() {
        logger.info(" Calling method deleteAll");
        service.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}