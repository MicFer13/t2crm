package com.assignment.crm.services.user;

import com.assignment.crm.dao.IUserDAO;
import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;
import com.assignment.crm.exceptions.CustomExceptions;
import com.assignment.crm.utility.UserHelper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService  {

    private IUserDAO iUserDAO;
    private UserHelper userHelper;

    public UserService(IUserDAO iUserDAO, UserHelper userHelper){
        this.iUserDAO =  iUserDAO;
        this.userHelper = userHelper;
    }

    @Override
    public List<User> findByFirstName(String firstname) {
        if(firstname.isEmpty() || firstname == null) throw new CustomExceptions("First Name cannot be null or empty");
        return iUserDAO.findByFirstName(firstname);
    }

    @Override
    public List<User> findByStatus(String status) {
        if(status.isEmpty() || status == null) throw new CustomExceptions("Status cannot be null or empty");
        return iUserDAO.findByStatus(status);
    }

    @Override
    public void save(UserFromUI userFromUI) {
        User user = userHelper.combineUIandFullUser(userFromUI);
        iUserDAO.save(user);
    }

    @Override
    public List<User> findAllUsers() {
        return iUserDAO.findAllUsers();
    }

    @Override
    public User findById(int id) {
        return iUserDAO.findById(id);
    }

    @Override
    public void updateUser(User user) {
        iUserDAO.updateUser(user);
    }

    @Override
    public void deleteUserById(int id) {
        if(id < 0) throw new CustomExceptions("Id cannot be less than zero");
        iUserDAO.deleteUserById(id);
    }

    @Override
    public void deleteAll() {
        iUserDAO.deleteAll();
    }
}