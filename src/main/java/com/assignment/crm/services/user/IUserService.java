package com.assignment.crm.services.user;

import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserFromUI;

import java.util.List;

public interface IUserService {
    List<User> findAllUsers();
    User findById(int id);
    List<User> findByFirstName(String firstname);
    List<User> findByStatus(String status);
    void save(UserFromUI userFromUI);
    void updateUser(User user);
    void deleteUserById(int id);
    void deleteAll();

}
