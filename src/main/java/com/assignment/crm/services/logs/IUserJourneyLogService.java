package com.assignment.crm.services.logs;

import com.assignment.crm.entities.Logs;
import com.assignment.crm.entities.UserJourneyLog;

import java.util.List;

public interface IUserJourneyLogService {
    List<UserJourneyLog> retrieveAllLogsById(int id);
    List<Logs> retrieveParselog(int id);
}
