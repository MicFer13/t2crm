package com.assignment.crm.services.logs;

import com.assignment.crm.dao.IUserJourneyLogDAO;
import com.assignment.crm.entities.Logs;
import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.exceptions.CustomExceptions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class UserJourneyLogService implements IUserJourneyLogService {

    private IUserJourneyLogDAO iUserJourneyLogDAO;

    public UserJourneyLogService(IUserJourneyLogDAO iUserJourneyLogDAO){
        this.iUserJourneyLogDAO = iUserJourneyLogDAO;
    }

    @Override
    public List<UserJourneyLog> retrieveAllLogsById(int id)
    {
        if(id < 0) throw new CustomExceptions("Id cannot be less than zero");

        return iUserJourneyLogDAO.retrieveAllLogsById(id);
    }

    @Override
    public List<Logs> retrieveParselog(int id)
    {
        if(id < 0) throw new CustomExceptions("Id cannot be less than zero");

        String log = iUserJourneyLogDAO.retrieveAllLogsById(id).get(0).getCommentary();
        log = log.replace("[","");
        log = log.replace("]",",");
        log = log.replace("{","");
        log = log.replace("}","");
        log = log.replace("ValueChange","");
        String [] items = log.split(",");

        List<Logs> myLogList = new ArrayList<Logs>(items.length);

        for (int i = 0 ; i < items.length ; i++) {
            Logs mylog = new Logs();
            if(items[i].startsWith("Date") && items[i+1].length()<3){
                continue;
            }
            if(items[i].length()<3){
                continue;
            }
            mylog.setActions(items[i]);
            myLogList.add(mylog);
        }
        return myLogList;
    }
}
