package com.assignment.crm.exceptions;

public class CustomExceptions extends RuntimeException {
    private static final long serialVersionUID = 3538181174213580960L;

    public CustomExceptions(String message)
    {
        super(message);
    }
    }