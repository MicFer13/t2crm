pipeline {

    agent any
    stages {
    stage('Build') {
        steps {


            sh 'gradle clean build'
        }
    }

        stage('Test') {
            steps {

                
                sh 'gradle jacocoTestReport'
            }
        }

        stage('Delivery') {
            steps {
                fileOperations([fileCopyOperation(
                        excludes: '',
                        flattenFiles: false,
                        includes: 'build\\libs\\**',
                        targetLocation: "C:\\\\JavaProject\\out"
                )])

            }
        }

    }
}
